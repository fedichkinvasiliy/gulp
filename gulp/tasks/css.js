module.exports = function () {
  $.gulp.task('css', function () {
    return $.gulp.src(['build/css/**/*.css'])
        .pipe($.gp.plumber())
        .pipe($.gp.csso())
        .pipe($.gulp.dest('build/css'))
        .pipe($.browserSync.stream());
  });
};