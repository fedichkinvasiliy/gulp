module.exports = function () {
  $.gulp.task('copy', function () {
    return $.gulp.src([
      'src/fonts/**/*.{woff,woff2}',
      'src/img/**/*.{jpg,png,svg}',
      'src/*.html',
      'src/css/*.css'
    ], {
      base: 'src'
    })
      .pipe($.gulp.dest('build'));
  });
};