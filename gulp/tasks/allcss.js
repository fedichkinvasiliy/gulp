module.exports = function () {
  $.gulp.task('allcss', function () {
    return $.gulp.src('src/css/*.{css}')
      .pipe($.gulp.dest('build/css'));
  });
};