module.exports = function () {
  $.gulp.task('serve', function () {
    $.browserSync.init({
      server: 'build/'
    });
    $.gulp.watch('src/css/**/*.css', $.gulp.series('allcss'));
    $.gulp.watch('src/*.html', $.gulp.series('html'));
    $.gulp.watch('src/js/**/*.js', $.gulp.series('scripts'));
    $.gulp.watch('src/img/**/*.{png,jpg,svg}', $.gulp.series('allimg'));
  });
};